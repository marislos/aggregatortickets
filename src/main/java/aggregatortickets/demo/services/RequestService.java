package aggregatortickets.demo.services;

import aggregatortickets.demo.entities.Request;

import java.util.List;
import java.util.Optional;

public interface RequestService {
    List<Request> findAll();
    Request findById(long id);
    Request save(Request request);
    void deleteById(long id);
}

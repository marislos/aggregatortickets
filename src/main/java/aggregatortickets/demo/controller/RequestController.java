package aggregatortickets.demo.controller;

import aggregatortickets.demo.entities.Request;
import aggregatortickets.demo.services.RequestService;
import aggregatortickets.demo.services.RequestServiceImpl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/requests")
public class RequestController {

    private final RequestService requestService;

    public RequestController(RequestServiceImpl requestService) {
        this.requestService = requestService;
    }

    @GetMapping("")
    public String findAll(Model model) {
        List<Request> requests = requestService.findAll();
        model.addAttribute("requests", requests);
        return "request";
    }

    @GetMapping("/add")
    public String newRequest(Model model) {
        model.addAttribute("request", new Request());
        return "add_request";
    }

    @PostMapping("/add")
    public String addingRequest(@ModelAttribute Request request, Model model) {
        model.addAttribute("request", request);
        requestService.save(request);
        model.addAttribute("request", new Request());
        return "add_request";
    }

}

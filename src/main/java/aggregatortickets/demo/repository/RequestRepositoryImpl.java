package aggregatortickets.demo.repository;

import aggregatortickets.demo.entities.Request;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class RequestRepositoryImpl implements RequestRepository{

    private Map<Long, Request> requests = new HashMap<>();
    private long requestId = 0;


    @Override
    public List<Request> findAll() {
        List<Request> requestList = new ArrayList<>();
        for (long k : requests.keySet())
        {
            requestList.add(requests.get(k));
        }
        return requestList;
    }

    @Override
    public Request findById(long id) {
        return requests.get(id);
    }

    @Override
    public Request save(Request request) {
        for (long k : requests.keySet()) {
            if (k == request.getId()) {
                return requests.replace(k, request);
            }
        }
        requestId++;
        return requests.put(requestId, request);

    }

    @Override
    public void deleteById(Long id) {
        requests.entrySet().removeIf(entry -> entry.getKey().equals(id));
    }
}

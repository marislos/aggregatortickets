package aggregatortickets.demo.repository;

import aggregatortickets.demo.entities.Request;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RequestRepository {
    List<Request> findAll();
    Request findById(long id);
    Request save(Request request);
    void deleteById(Long id);
}

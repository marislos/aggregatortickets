package aggregatortickets.demo.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Request {
    private long id;

    private String MuseumName;
}

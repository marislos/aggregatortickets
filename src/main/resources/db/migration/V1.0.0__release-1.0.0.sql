create table role
(
    id   bigserial primary key,
    name varchar unique not null
);

create table users
(
    id       bigserial primary key,
    login    varchar unique not null,
    password varchar unique not null,
    pass_id  bigint unique references role (id)
);

create table schedule
(
    id   bigserial primary key,
    name varchar unique not null
);

create table museum
(
    id          bigserial primary key,
    name        varchar unique not null,
    schedule_id bigint unique references schedule (id)
);

create table requests
(
    id          bigserial primary key,
    museum_id   bigint unique references museum (id),
    schedule_id bigint unique references schedule (id)
);